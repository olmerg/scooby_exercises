Programming in ROS2
===================

Os tutorias deste capítulo são um simplificação dos apresentados no
[link tutorias ros2 begginer](https://index.ros.org/doc/ros2/Tutorials/#beginner-client-libraries).

Create Workspace
----------------

-   Instalar os programas requeridos para compilar

    `sudo apt install python3-colcon-common-extensions`

    `sudo apt-get install python3-rosdep2 `

    recomendados `sudo apt-get install git python3-vcstools`

-   Criar a pasta do espaço de trabalho (workspace)

    `mkdir -p  /lma\_ws/src `

    `cd  /lma\_ws/src`

-   Build

    `colcon build`

- Deixar que o linux possa procurar na pasta do workspace:
  `echo "source ~/lma_ws/install/setup.bash" >> ~/.bashrc`

    ou

  `cd ~/lma_ws`
  `. /install/setup.bash`

Qualquer dúvida pode ir ao tutorial original [Creating a workspace](https://index.ros.org/doc/ros2/Tutorials/Workspace/Creating-A-Workspace).
Recomendado ler sobre Underlay Vs overLay.

Create a package
----------------

Um pacote pode ser considerado um contêiner para o seu código ROS 2. Se
você deseja instalar seu código ou compartilhá-lo com outras pessoas,
precisará organizá-lo em um pacote. Com os pacotes, você pode liberar o
trabalho do ROS 2 e permitir que outras pessoas o construam e usem com
facilidade. Na figura seguinte tem a estrutura de arquivos de um pacote
dentro do *workspace*.

![Exemplo estrutura dos pacotes.](../figures/pacote.png)

-   cada pacote é uma pasta que deve estar no *src* do *workspace*

        cd ~/lms_ws/src
            

-   Criar o pacote

        ros2 pkg create --build-type ament_cmake basic_examples
            

-   configurar o arquivo *package.xml* com os dados sobre o pacote e o
    desenvolvedor

    ``` {.xml}
    <name>basic_examples</name>
    <version>0.0.0</version>
    <description>TODO: Package description</description>
    <maintainer email="robot@todo.todo">robot</maintainer>
    <license>TODO: License declaration</license>
    ```

-   compilar

        cd ~\lma_ws
        colcon build
            

-   adicionar ao bash

        cd ~/lma_ws
        . /install/setup.bash
            

-   Procurar o pacote (Com este comando vai listar todos os pacotes do ros2 instalados)

        ros2 pkg list
            

Qualquer duvida pode ir ao tutorial original [Creating your first ROS 2
package](https://index.ros.org/doc/ros2/Tutorials/Creating-Your-First-ROS2-Package/)

Create a simple publisher/subscriber
------------------------------------

Nós são processos executáveis que se comunicam pelo gráfico ROS. Neste tutorial, os nós passarão informações na forma de mensagens de string para um tópico. O exemplo usado aqui é um sistema simples de “conversador” e “ouvinte”; um nó publica dados e o outro se inscreve no tópico para que possa receber esses dados.

### Publisher

-   Ir ao pacote criado no tuto anterior

        cd ~/lma_ws/src/basic_examples
        mkdir src
        cd src
            

-   Nesta pasta criar o arquivo *publisher.cpp*

    ``` {.c++}
    #include <chrono>
    #include <functional>
    #include <memory>
    #include <string>

    #include "rclcpp/rclcpp.hpp"
    #include "std_msgs/msg/string.hpp"

    using namespace std::chrono_literals;

    class MinimalPublisher : public rclcpp::Node
    {
      public:
        MinimalPublisher()
        : Node("minimal_publisher"), count_(0)
        {
          publisher_ = this->create_publisher<std_msgs::msg::String>("topic", 10);
          timer_ = this->create_wall_timer(
          500ms, std::bind(&MinimalPublisher::timer_callback, this));
        }

      private:
        void timer_callback()
        {
          auto message = std_msgs::msg::String();
          message.data = "Hello, world! " + std::to_string(count_++);
          RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", message.data.c_str());
          publisher_->publish(message);
        }
        rclcpp::TimerBase::SharedPtr timer_;
        rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
        size_t count_;
      };

      int main(int argc, char * argv[])
      {
        rclcpp::init(argc, argv);
        rclcpp::spin(std::make_shared<MinimalPublisher>());
        rclcpp::shutdown();
        return 0;
      }
    ```

-   Configurar o *package.xml*

    ``` {.xml}
    <depend>rclcpp</depend>
    <depend>std_msgs</depend>
    ```

-   Configurar o *CMakelist.txt*

    ``` {.cmake}
    ...
      find_package(rclcpp REQUIRED)
      find_package(std_msgs REQUIRED)
     ...
    add_executable(publisher src/publisher.cpp)
    ament_target_dependencies(publisher rclcpp std_msgs)
    ...
      install(TARGETS
        publisher
        DESTINATION lib/${PROJECT_NAME})
    ```

### subscriber

-   Ir ao pacote criado no tuto anterior

             cd ~/lma_ws/src/basic_examples
             mkdir src
             cd src
            

-   Nesta pasta criar o arquivo *subscriber.cpp*

    ``` {.c++}
    #include <memory>

    #include "rclcpp/rclcpp.hpp"
    #include "std_msgs/msg/string.hpp"
    using std::placeholders::_1;

    class MinimalSubscriber : public rclcpp::Node
    {
      public:
        MinimalSubscriber()
        : Node("minimal_subscriber")
        {
          subscription_ = this->create_subscription<std_msgs::msg::String>(
          "topic", 10, std::bind(&MinimalSubscriber::topic_callback, this, _1));
        }

      private:
        void topic_callback(const std_msgs::msg::String::SharedPtr msg) const
        {
          RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg->data.c_str());
        }
        rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
    };

    int main(int argc, char * argv[])
    {
      rclcpp::init(argc, argv);
      rclcpp::spin(std::make_shared<MinimalSubscriber>());
      rclcpp::shutdown();
      return 0;
    }
    ```

-   O *package.xml* não tem que ser modificado.

-   Configurar o *cmakelist.txt*

    ``` {.cmake}
    .
     ...
    add_executable(subscriber src/subscriber.cpp)
    ament_target_dependencies(subscriber rclcpp std_msgs)
    ...
      install(TARGETS
        suscriber
        DESTINATION lib/${PROJECT_NAME})
    ```

Qualquer dúvida pode ir ao tutorial original [Writing a simple publisher and subscriber (C++)](https://index.ros.org/doc/ros2/Tutorials/Writing-A-Simple-Cpp-Publisher-And-Subscriber/)

Create a simple service(server/client)
--------------------------------------

Ir ao tutorial original [Writing a simple service and client
(C++)](https://index.ros.org/doc/ros2/Tutorials/Writing-A-Simple-Cpp-Service-And-Client/)

Create customs interfaces 
--------------------------

Ir ao tutorial original [Creating custom ROS 2 msg and srv files](https://index.ros.org/doc/ros2/Tutorials/Custom-ROS2-Interfaces/)

Create a launch
---------------

TODO Ir ao tutorial original [Creating a launch
file](https://index.ros.org/doc/ros2/Tutorials/Launch-Files/Creating-Launch-Files/)

Create a publisher for Samsung robot
====================================

Para fazer esta parte e necessário você adicionar a pasta do workspace (*~/lma_ws/src*) a pasta *scooby_simulation* e ter os programas requeridos de acordo ao tutorial feito de simulação.

Programação
-----------

Iniciaremos a partir do arquivo de publisher.cpp e seguir os seguintes
passos:

-   modificamos nome do arquivo circle\_scooby.cpp

-   adicionar a librária do topico desejado a publicar

    ``` {.c++}
    #include <geometry_msgs/msg/twist.hpp>
    ```

-   cambiar nome do nó:

        Node("scooby_circle")

-   modificar no construtor da classe o tipo de mensagens e na criação
    do *publisher\_*

``` {.c++}
publisher_ = this-create_publisher<geometry_msgs::msg::Twist>("scooby/cmd_vel", 10);
...
rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
```

-   Modificar o método do timer *timer\_callback()* para fazer que o     robô tenha uma velocidade linear e uma velocidade angular fixa,assim     nos vamos a conseguir ele fazer um circulo

``` {.c++}
    void timer_callback()     {
      auto message = geometry_msgs::msg::Twist();
      message.linear.x = 0.5; 
      message.angular.z = 0.5;
      RCLCPP_INFO(this->get_logger(), "linear vel: '%.2f, angular vel: %.2f",                   message.linear.x,message.angular.z);       
      publisher_->publish(message);
    }
```

Compilar
--------

Para compilar requeremos:

-   Modificar o arquivo package.xml o qual e o encargado de descrever     para as ferramentas de ros quais são as dependências do projeto, neste caso vamos a adicionar o pacote geometry\_msgs, adicionando

    ``` {.xml}
    <depend>geometry_msgs</depend>
    ```

-   Vamos a falar com o compilador a traves do CMakeLists.txt:
    -   Adicionar a libraria do pacote geometry_msgs:
    -   Adicionar o criar o executável com suas dependências:
        ``` {.cmake}
        find_package(geometry_msgs
        REQUIRED)
        ...
        add_executable(circle_scooby src/circle_scooby.cpp) 
        ament_target_dependencies(circle_scooby rclcpp geometry_msgs)
        ...
        install(TARGETS circle_scooby DESTINATION lib/\$\{PROJECT_NAME\})
        ```
-   Vamos a compilar o programa (Na consola)

    ``` 
    cd lma_ws
    colcon build
    ```
Se todo foi correto deveria sair na consola Finished \<\<\<basic\_examples

Execução
--------

Vamos ter duas consolas uma para a simulação e outra para nosso nó:

``` 
cd lma_ws
. install/setup.bash
ros2 launch scooby_gazebo scooby.launch.py world:=scooby_empty.world
```

**Consola dos**

``` {.c++}
cd lma_ws
. install/setup.bash
ros2 run basic_examples circle_scooby
```
No caso que o simulador nao estaja funcionando você pode utilizar o *turtlesim*

    ros2 run turtlesim turtlesim_node --ros-args --remap turtle1/cmd_vel:=scooby/cmd_vel


### Fazer um launch

Os arquivos Launch permitem que você inicialize e configure vários executáveis contendo nós ROS 2 simultaneamente.
- Criar a pasta launch
```
cd ~lma_ws\src\basic_examples
mkdir launch
```
- Criar o arquivo texte.launch.py

```
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='turtlesim',
            executable='turtlesim_node',
            name='sim',
            remappings=[
                ('turtle1/cmd_vel', 'scooby/cmd_vel')
            ]
        ),
        Node(
            package='basic_examples',
            #namespace='scooby',
            executable='circle_scooby',
            name='circle',
            parameters=[{'vel_scooby': 2.0 }]
        )
    ])
```
- Modificar o CMakeLists para adicionar o folder launch ao instalar o pacote 

```
install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}/
)
```

### Lista

**Exercicio 1.** utilizar a variável count\_ para que depois de certo tempo o robô para o girar no sentido inverso

``` {.c++}
    void timer_callback()     {
      auto message = geometry_msgs::msg::Twist();
      if(count_<200){         
        message.linear.x = 0.5;
        message.angular.z = 0.5;
      }else{
        message.linear.x = 0.0;
        message.angular.z = 0.0;
      }   
        publisher_->publish(message);
        count_++;
    }
```

**Exercicio 2.** Modificar as velocidades como um parametro (vel\_scooby) para que seja possivel trocar os valores pelo tutorial [Using parameters in a class c++](https://index.ros.org/doc/ros2/Tutorials/Using-Parameters-In-A-Class-CPP/).

``` {.c++}
...
MinimalPublisher()     : Node("scooby_circle"), count_(0)     { 
...
      this->declare_parameter<double>("vel_scooby", 1.0);
...
   
void timer_callback()     {       
      auto message = geometry_msgs::msg::Twist();
      double parameter_vel_;
      this->get_parameter("vel_scooby", parameter_vel_);              
    if(count_<200){
        message.linear.x = 0.5*parameter_vel_;
        message.angular.z = 0.5*parameter_vel_;
      }else{
    ...
  }
```

**Exercicio 3.** Fazer uma suscribção ao topic */scooby/sensors/lasers/rear_left_data* para parar o robo quando achar algum elemento perto.

``` {.c++}
#include <sensor_msgs/msg/laser_scan.hpp>
using std::placeholders::_1;
...
MinimalPublisher()     : Node("sooby_circle"), count_(0)     { 
...
      auto default_qos = rclcpp::QoS(rclcpp::SensorDataQoS());
      subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "/scooby/sensors/lasers/rear_left_data",  
        default_qos,std::bind(&MinimalPublisher::read_laser, this, _1));
...
  void read_laser(const sensor_msgs::msg::LaserScan::SharedPtr _msg){
      auto medicao = _msg->ranges[0];
      if(medicao<10){
      RCLCPP_INFO(this->get_logger(),"Stooping by distance: %f",medicao);
      count_=1000;
      auto message = geometry_msgs::msg::Twist();
      message.linear.x = 0.0;
      message.angular.z = 0.0;
      publisher_->publish(message);
      }
  }
...
```

Adicionar no *package.xml* e *CMakelists* o pacote *sensor\_msgs* e depois compilar

Aqui neste exemplo e importante o [perfil de qualidade do serviço do
tópico](https://index.ros.org//doc/ros2/Concepts/About-Quality-of-Service-Settings).

![[Fatores de qualidade do serviço predefinidos](http://docs.ros2.org/foxy/api/rclcpp/classrclcpp_1_1QoS.html)](../figures/classrclcpp_1_1QoS__inherit__graph.png)


**Exercicio 4.** Modificar Launch para funcionar com scooby

```
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
import os

def generate_launch_description():
    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')
    pkg_scooby_gazebo = get_package_share_directory('scooby_gazebo')
    
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gazebo.launch.py'),
        )
    )
    return LaunchDescription([
        DeclareLaunchArgument(
          'world',
          default_value=[os.path.join(pkg_scooby_gazebo, 'worlds', 'scooby_empty.world'), ''],
          description='SDF world file'),
        gazebo,
        Node(
            package='basic_examples',
            #namespace='scooby',
            executable='circle_scooby',
            name='circle',
            parameters=[{'vel_scooby': 2.0 }]
        )    
        ])
```
execuçao:

  cd lma_ws
  colcon build
  . /usr/share/gazebo/setup.sh
  . install/setup.bash
  ros2 launch basic_examples scooby.launch.py

Neste exercicio nos utilizamos os comando IncludeLaunchDescription para adicionar o launch ja existente que inicializa o gazebo clinet(interface) eo gzserver(o que faz os calculos). Este arquivo tem multiples configuraçoes, neste exemplo vamos só a utilizar o parametro 'world' para falar que nos vamos a abrir o world scooby_empty.world. Ao igual que no launch do scooby_gazebo nos podemos trocar este parametro executando o seguinte
  ros2 launch basic_examples scooby.launch.py world:=scooby_factory_v2.world